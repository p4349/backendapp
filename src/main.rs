use std::{io::Result};
use actix_web::{App, HttpServer, web, http, middleware::Logger};
use actix_cors::Cors;

use photo_back::{
    config,
    modules::{photos},
    state
};


#[actix_web::main]
async fn main() -> Result<()> {
    let api_base = "/api";
    let config = config::Settings::new().unwrap();

    // Create Photo repo based in unsplash
    let unsplash_repo = photos::infra::unsplash::UnsplashRepo{
        access_key: config.unsplash.access_key
    };

    let server = match HttpServer::new( move || {
        let cors = Cors::default()
              .allowed_origin(&config.server.allowed_host)
              .allowed_methods(vec!["GET", "POST"])
              .allowed_header(http::header::CONTENT_TYPE)
              .max_age(3600);

        App::new()
            .wrap(cors)
            .data(state::AppState {
                photo_repo: Box::new(unsplash_repo.clone())
            })
            .wrap(Logger::default())
            .service(
                web::scope(&api_base)
                    .configure(photos::infra::endpoints::config)
            )
    })
    .bind(format!("{}:{}", config.server.host, config.server.port)) {
        Ok(server) => server,
        Err(e) => panic!("{}", e)
    };


    server.run().await
}

use crate::modules::photos::{
  domain::PhotoRepository
};

#[derive(Clone)]
pub struct UnsplashRepo {
  pub access_key: String
}

static BASE: &str = "https://api.unsplash.com";


impl PhotoRepository for UnsplashRepo {
  fn get_list_url(&self, page: u64) -> String {
    let endpoint = format!("{}/photos?order_by=popular&client_id={}&page={}", BASE, self.access_key, page);
    return endpoint;
  }

  fn get_search_url(&self, keyword: String) -> String {
    format!("{}/search/photos?query={}&client_id={}", BASE, keyword, self.access_key)
  }

  fn get_today_url(&self) -> String {
    format!("{}/photos?order_by=latest&client_id={}", BASE, self.access_key)
  }
}
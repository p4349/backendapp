use actix_web::{Responder, HttpRequest, HttpResponse, web};
use crate::{state::AppState};
use serde::{Deserialize};

#[derive(Debug, Deserialize)]
pub struct Params {
    page: u64,
}

async fn get_photos(state: web::Data<AppState>, req: HttpRequest) -> impl Responder {
    let repo = &state.photo_repo;
    let params = web::Query::<Params>::from_query(req.query_string()).unwrap();

    let response = reqwest::get(&repo.get_list_url(params.page)).await.unwrap();
    let photos = response.text().await.unwrap();

    HttpResponse::Ok()
        .content_type("application/json")
        .body(photos)
}

async fn search_photo(state: web::Data<AppState>, path: web::Path<(String,)>,) -> impl Responder {
    let search_query = path.into_inner();
    let repo = &state.photo_repo;
    let response = reqwest::get(&repo.get_search_url(search_query.0)).await.unwrap();
    let photos = response.text().await.unwrap();

    HttpResponse::Ok()
        .content_type("text/plain")
        .body(photos)
}

async fn today_photos(state: web::Data<AppState>) -> impl Responder {
    let repo = &state.photo_repo;
    let response = reqwest::get(&repo.get_today_url()).await.unwrap();
    let photos = response.text().await.unwrap();

    HttpResponse::Ok()
        .content_type("text/plain")
        .body(photos)
 }


pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/photo")
            .route(web::get().to(get_photos))
    )
    .service(
        web::resource("/photo/search/{search}")
            .route(web::get().to(search_photo))
    ).service(
        web::resource("/photo/today")
            .route(web::get().to(today_photos))
    );
}

pub struct Photo {
  pub id: i64,
  pub small_url: String,
  pub full_url: String,
}
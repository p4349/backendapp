pub trait PhotoRepository {
  fn get_list_url(&self, page: u64) -> String;
  fn get_today_url(&self) -> String;
  fn get_search_url(&self, keyword: String) -> String;
}
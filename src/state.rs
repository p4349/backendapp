use crate::{
  modules::{photos}
};


pub struct AppState {
    pub photo_repo: Box<dyn photos::domain::PhotoRepository>
}
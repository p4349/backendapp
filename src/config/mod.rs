use config::{ConfigError, Config, File};
use serde::{Deserialize};

#[derive(Debug, Deserialize)]
pub struct Server {
  pub host: String,
  pub port: u16,
  pub allowed_host: String
}

#[derive(Debug, Deserialize)]
pub struct Unsplash {
  pub access_key: String
}

#[derive(Debug, Deserialize)]
pub struct Settings {
  pub server: Server,
  pub unsplash: Unsplash
}

impl Settings {
  pub fn new() -> Result<Self, ConfigError> {
    let mut s = Config::default();
    s.merge(File::with_name("default.toml"))?;
    s.merge(File::with_name("config.toml").required(false))?;
    s.try_into()
  }
}
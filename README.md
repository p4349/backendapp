# PhotoFigma backend

Api backend to replicate photofigma prototype written in rust.

## Requirements
- cargo 1.56.0
- rustc 1.56.1

Please refer to [rust-lang site](https://www.rust-lang.org/learn/get-started) to install both of them.

## Config
You may see default.toml as a reference of the available coinfiguration. Please create a **config.toml** to override the configuration and use sensitive values.

## Endpoints
You may find in the insomnia directory all of the [insomnia](https://insomnia.rest/download)(v4JSON) backups.

## Development
This repository tries to follow hexagonal architecture and concepts like infrastructure, application and domain layers.

> To start a dev server please use
```bash
$ cargo run
```


## Production
> To compile a production server please use
```bash
$ cargo build --release
```
Then use the executable located inside target/release directory.
